package db.w4rlock.telugumovies.Models;

import java.util.ArrayList;


public class MoviePersons {
	public String name;
	public int id;
	public String profileImageUrl;
	public String profileImageUrlSmall;
	public ArrayList<String> professions;
	public String bio;
	public ArrayList<Movie> movies;
	public ArrayList<apiImageUrls> images;
	public ArrayList<apiImageUrls> getImages() {
		return images;
	}
	public void setImages(ArrayList<apiImageUrls> images) {
		this.images = images;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	public ArrayList<Movie> getMovies() {
		return movies;
	}
	public void setMovies(ArrayList<Movie> movies) {
		this.movies = movies;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProfileImageUrl() {
		return profileImageUrl;
	}
	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}
	public String getProfileImageUrlSmall() {
		return profileImageUrlSmall;
	}
	public void setProfileImageUrlSmall(String profileImageUrlSmall) {
		this.profileImageUrlSmall = profileImageUrlSmall;
	}
	public ArrayList<String> getProfessions() {
		return professions;
	}
	public void setProfessions(ArrayList<String> professions) {
		this.professions = professions;
	}
	
}
