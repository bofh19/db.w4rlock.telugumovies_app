package db.w4rlock.telugumovies.Models;

import java.util.ArrayList;


public class Movie {
	public String icon;
	public String icon_small;
	public String title;
	public int days_left;
	public ArrayList<String> actors;
	public Integer movie_id;
	public String backgroundImageUrl;
	public String info;
	public String plot;
	public ArrayList<apiImageUrls> images;
	public String status;
	public ArrayList<String> trailers;
	public ArrayList<String> getTrailers() {
		return trailers;
	}
	public void setTrailers(ArrayList<String> trailers) {
		this.trailers = trailers;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<apiImageUrls> getImages() {
		return images;
	}
	public void setImages(ArrayList<apiImageUrls> images) {
		this.images = images;
	}
	public String getIcon_small() {
		return icon_small;
	}
	public void setIcon_small(String icon_small) {
		this.icon_small = icon_small;
	}
	public String getPlot() {
		return plot;
	}
	public void setPlot(String plot) {
		this.plot = plot;
	}
	public ArrayList<MoviePersons> moviePersons;
	public Movie(){
		super();
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getDays_left() {
		return days_left;
	}
	public void setDays_left(int days_left) {
		this.days_left = days_left;
	}
	public ArrayList<String> getActors() {
		return actors;
	}
	public void setActors(ArrayList<String> actors) {
		this.actors = actors;
	}
	public Integer getMovie_id() {
		return movie_id;
	}
	public void setMovie_id(Integer movie_id) {
		this.movie_id = movie_id;
	}
	public String getBackgroundImageUrl() {
		return backgroundImageUrl;
	}
	public void setBackgroundImageUrl(String backgroundImageUrl) {
		this.backgroundImageUrl = backgroundImageUrl;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public ArrayList<MoviePersons> getMoviePersons() {
		return moviePersons;
	}
	public void setMoviePersons(ArrayList<MoviePersons> moviePersons) {
		this.moviePersons = moviePersons;
	}
	public Movie(String icon,String title,int days_left,ArrayList<String> actors,Integer movie_id){
		super();
		this.icon = icon;
		this.title = title;
		this.days_left = days_left;
		this.actors = actors;
		this.movie_id = movie_id;
	}
}
