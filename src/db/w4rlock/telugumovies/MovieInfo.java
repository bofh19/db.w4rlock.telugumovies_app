package db.w4rlock.telugumovies;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import db.w4rlock.telugumovies.Adapters.MainImageAdapter;
import db.w4rlock.telugumovies.Adapters.MovieInfoPersonsAdapter;
import db.w4rlock.telugumovies.Models.Movie;
import db.w4rlock.telugumovies.Models.MoviePersons;
import db.w4rlock.telugumovies.Models.apiImageUrls;
import db.w4rlock.telugumovies.utils.FileCache;
import db.w4rlock.telugumovies.utils.ImageLoader;
import db.w4rlock.telugumovies.utils.getURLdata;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MovieInfo extends Activity {

	public final static String movie_api_server = "/mobile/v1/api/m";
	public final static String image_api_server = "/mobile/v1/api/m/images";
	public final static String PERSON_ID_MESSAGE = "db.w4rlock.telugumovies.MESSAGE";
	public final static String IMAGE_URLs_MESSAGE = "db.w4rlock.telugumovies.IMAGE_URLs_MESSAGE";
	public final static String UPDATE_ONLINE_MESSAGE = "db.w4rlock.telugumovies.UPDATE_ONLINE_MESSAGE";
	public final static String IMAGE_URLs_ID = "db.w4rlock.telugumovies.IMAGE_URLs_ID";
	public String[] intent_messages = null;
	public String server = "";
	public TextView movie_title;
	public String image_server = null;
	public Movie movie;

	public static final String DEBUG_TAG = "server reponse";
	public ImageLoader imageLoader;
	public ViewPager pager = null;
	FileCache fileCache;
	public File fileMovieInfo = null;
	public File fileMovieImages = null;
	public Integer this_movie_id = null;
	public String api_server;
	public Boolean update_info = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_movie_info);
		Intent intent = getIntent();
		intent_messages = intent
				.getStringArrayExtra(MainActivity.MOVIE_ID_MESSAGE);
		this_movie_id = Integer.parseInt(intent_messages[0]);
		server = new String(ServerVariables.server);

		api_server = new String();
		api_server = server + movie_api_server + "/" + this_movie_id + "/";
		image_server = server + image_api_server + "/" + this_movie_id + "/";
		Boolean filechecks = false;
		Boolean cachecheck = false;
		movie_title = (TextView) findViewById(R.id.movie_title);
		movie_title.setText(api_server);
		movie = new Movie();
		imageLoader = new ImageLoader(this.getApplicationContext());
		this.setTitle("movie title");
		try {
			fileCache = new FileCache(getApplicationContext());
			fileMovieInfo = fileCache.getFile(api_server);
			fileMovieImages = fileCache.getFile(image_server);
			filechecks = true;
		} catch (Exception e) {
			System.out.println(e);
		}

		if (filechecks) {
			try {
				cachecheck = runfromfileMovieInfo(fileMovieInfo);
			} catch (IOException e) {
				System.out.println("runfromfileerror1");
				e.printStackTrace();
			}
		}
		if (!cachecheck) {
			System.out.println("in network " + !cachecheck);
			ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				movie_title.setText("loading data...");
				try {
					movie_title.setText("contacting server");
					new DownloadWebpageText().execute(api_server);
				} catch (Exception e) {
					System.out.println(e);
					movie_title.setText("Error On Getting Data");
				}
			} else {
				movie_title.setText("no network connection avilable");
			}
		}
	}

	private static String readFile(String path) throws IOException {
		FileInputStream stream = new FileInputStream(new File(path));
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}

	private Boolean runfromfileMovieInfo(File fileMovieInfo2)
			throws IOException {
		String res1 = "";
		if (fileMovieInfo2.exists()) {
			FileReader fr = new FileReader(fileMovieInfo2);
			if (fr.read() == -1) {
				System.out.println("EMPTY");
				return false;
			} else {
				try {
					res1 = readFile(fileMovieInfo2.getAbsolutePath());
				} catch (IOException e) {
					System.out
							.println("error while reading cache file after its not empty "
									+ e);
					return false;
				}
			}
		} else {
			System.out.println("DOES NOT EXISTS");
			return false;
		}
		postresult(res1);
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_movie_info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_movie:
			try {
				pager.setCurrentItem(1);
			} catch (Exception e) {
				System.out.println("Error while changing page: " + e);
			}
			return true;
		case R.id.menu_cast:
			try {
				pager.setCurrentItem(2);
			} catch (Exception e) {
				System.out.println("Error while changing page: " + e);
			}
			return true;
		case R.id.menu_rating:
			try {
				pager.setCurrentItem(4);
			} catch (Exception e) {
				System.out.println("Error while changing page: " + e);
			}
			return true;
		case R.id.menu_images:
			try {
				pager.setCurrentItem(3);
			} catch (Exception e) {
				System.out.println("Error while changing page: " + e);
			}
			return true;
		case R.id.menu_main_page:
			try {
				Intent intent = new Intent(this, MainActivity.class);
				startActivity(intent);
			} catch (Exception e) {
				System.out.println("Error while changing activity: " + e);
			}
			return true;
		case R.id.menu_index:
			try {
				Intent intent = new Intent(this, HomePage.class);
				startActivity(intent);
			} catch (Exception e) {
				System.out.println("Error while changing acitivty: " + e);
			}
			return true;
		case R.id.menu_update:
			updateinfo();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void updateinfo() {
		System.out.println("in network from menu");
		update_info = true;
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			movie_title.setText("loading data...");
			try {
				movie_title.setText("contacting server");
				new DownloadWebpageText().execute(api_server);
			} catch (Exception e) {
				System.out.println(e);
				movie_title.setText("Error On Getting Data");
			}
		} else {
			movie_title.setText("no network connection avilable");
		}
	}

	public void postresult(Object result) {
		JSONObject thisobj = new JSONObject();
		String res = (String) result;
		try {
			thisobj = new JSONObject(res);
		} catch (Exception e) {
			System.out.println("error while casting to object: " + e);
		}
		String plot = "";
		String title = "";
		String info = "";
		String status = "";
		int days_left;
		try {
			plot = (String) thisobj.get("plot");
			info = thisobj.getString("info");
			title = (String) thisobj.get("name");
			status = (String) thisobj.get("status");
			String backgroundImageUrl = new String(
					(String) thisobj.get("background_image"));
			backgroundImageUrl = server + backgroundImageUrl;
			movie.setBackgroundImageUrl(backgroundImageUrl);
		} catch (Exception e) {
			System.out.println("error while getting data from obj: " + e);
		}
		try{
			days_left = thisobj.getInt("days_left");
			movie.setDays_left(days_left);
		}catch(Exception e){
			System.out.println("Error while getting date from obj: "+e);
		}
		try {
			movie_title.setText(Html.fromHtml(plot));
			movie.setPlot(plot);
			movie.setInfo(info);
			movie.setStatus(status);
			movie.setTitle(title);
			System.out.println("in parser");
			System.out.println(movie.getTitle());
			System.out.println(movie.getDays_left());
			System.out.println(movie.getStatus());
			this.setTitle(title);
			// System.out.println("Got data in postresult");

		} catch (Exception e) {
			System.out.println("error while marking them on page: " + e);
		}

		try {
			View loadingbar = (View) findViewById(R.id.progress_large);
			loadingbar.setVisibility(View.GONE);
			movie_title.setVisibility(View.GONE);
		} catch (Exception e) {
			System.out.println("Error while removin loading bar: " + e);
		}
		
		try{
			JSONArray trailersArray = new JSONArray();
			trailersArray = (JSONArray)thisobj.get("trailers");
			ArrayList<String> movieTrailers = new ArrayList<String>();
			for(int x=0;x<trailersArray.length();x++){
				String cur_trailer_str = new String();
				JSONObject current_trailer = trailersArray.getJSONObject(x);
				cur_trailer_str = current_trailer.getString("youtube_code");
//				cur_trailer_str = ServerVariables.youtube_image_url_start+cur_trailer_str+ServerVariables.youtube_image_url_end;
//				System.out.println(cur_trailer_str);
				movieTrailers.add(cur_trailer_str);
			}
			movie.setTrailers(movieTrailers);
		}catch(Exception e){
			System.out.println("Error while getting trailers data: "+e);
		}

		try {
			JSONArray personsArray = new JSONArray();
			personsArray = (JSONArray) thisobj.get("persons");
			ArrayList<MoviePersons> moviePersons = new ArrayList<MoviePersons>();
			for (int x = 0; x < personsArray.length(); x++) {
				MoviePersons currentMoviePerson = new MoviePersons();

				JSONObject currentMoviePersonJsonObject = personsArray
						.getJSONObject(x);
				int id = currentMoviePersonJsonObject.getInt("id");
				String name = currentMoviePersonJsonObject.getString("name");
				String profileImageUrl = currentMoviePersonJsonObject
						.getString("profile_image");
				profileImageUrl = server + profileImageUrl;
				String profileImageUrlSmall = currentMoviePersonJsonObject
						.getString("profile_image_small");
				profileImageUrlSmall = server + profileImageUrlSmall;
				ArrayList<String> professions = new ArrayList<String>();
				JSONArray professionsJsonArray = currentMoviePersonJsonObject
						.getJSONArray("professions");
				for (int prof = 0; prof < professionsJsonArray.length(); prof++) {
					JSONObject thisprofobj = professionsJsonArray
							.getJSONObject(prof);
					professions.add(thisprofobj.getString("name"));
				}
				currentMoviePerson.setId(id);
				currentMoviePerson.setName(name);
				currentMoviePerson.setProfileImageUrl(profileImageUrl);
				currentMoviePerson
						.setProfileImageUrlSmall(profileImageUrlSmall);
				currentMoviePerson.setProfessions(professions);

				// System.out.println(currentMoviePerson.id);
				// System.out.println("current movie person: "+currentMoviePerson.name);
				// System.out.println(currentMoviePerson.profileImageUrl);
				// System.out.println(currentMoviePerson.profileImageUrlSmall);
				// System.out.println(currentMoviePerson.getProfessions());
				moviePersons.add(currentMoviePerson);

			}
			movie.setMoviePersons(moviePersons);
			pager = (ViewPager) findViewById(R.id.viewpager);
			pager.setAdapter(new SampleAdapter());
			pager.setOffscreenPageLimit(6);
		} catch (Exception e) {
			System.out.println("error while getting persons: " + e);
		}

	}

	public ArrayList<apiImageUrls> parseimages(Object result2) {
		String res = (String) result2;
		JSONObject thisobj = new JSONObject();
		try {
			thisobj = new JSONObject(res);
		} catch (Exception e) {
			System.out.println("error while parsing images " + e);
		}
		try {
			JSONArray iarray = thisobj.getJSONArray("images");
			ArrayList<apiImageUrls> imageurlslist = new ArrayList<apiImageUrls>();
			for (int x = 0; x < iarray.length(); x++) {
				apiImageUrls currentimage = new apiImageUrls();
				JSONObject currentobj = iarray.getJSONObject(x);
				currentimage.setImageUrlSmall(currentobj.getString("small"));
				currentimage.setImageUrlActual(currentobj.getString("actual"));
				// System.out.println(currentobj.getString("actual"));
				imageurlslist.add(currentimage);
			}
			movie.setImages(imageurlslist);
			return imageurlslist;
		} catch (Exception e) {
			System.out.println("error while getting images array " + e);
		}
		return null;
	}

	private class DownloadWebpageText extends AsyncTask<Object, Object, Object> {

		@Override
		protected Object doInBackground(Object... params) {
			String data = (String) params[0];
			try {
				return downloadUrl(data);
			} catch (IOException e) {
				return "unable to get the data sry :(";
			}
		}

		@Override
		protected void onPostExecute(Object result) {
			if (result == null || result == " " || result == "") {
				movie_title.setText("result is broken are you online ?");
			} else {
				postresult(result);
				// System.out.println("got data on postexceute");

			}
		}

		private String downloadUrl(String myurl) throws IOException {
			InputStream is = null;

			try {
				System.out.println(myurl);
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);
				// Starts the query
				try {
					conn.connect();
				} catch (Exception e) {
					System.out.println("Error while connection: " + e);
					return " ";
				}
				System.out.println("connection request above");
				int response = conn.getResponseCode();
				Log.d(DEBUG_TAG, " " + response);
				try {
					is = conn.getInputStream();
				} catch (Exception e) {
					System.out
							.println("error while getting inputstream in do background: "
									+ e);
					return " ";
				}
				String contentAsString = readAll(is);
				return contentAsString;

			} catch (Exception e) {
				System.out.println(e);
				System.out.println("x");
				return " ";
			} finally {
				if (is != null) {
					is.close();
				} else {
					return null;
				}
			}
		}

		public String readAll(InputStream stream) throws IOException,
				UnsupportedEncodingException {
			StringBuilder sb = new StringBuilder();
			String s;
			try {
				Reader reader = new InputStreamReader(stream, "UTF-8");
				BufferedReader buf = new BufferedReader(reader);
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);
				}
				try {
					addtoCacheFile(sb.toString(), fileMovieInfo);
				} catch (Exception e) {
					System.out.println(e);
				}
				return sb.toString();
			} catch (Exception e) {
				System.out.println("error on readall ie catching input stream");
				return " ";
			}
		}
	}

	// Create a message handling object as an anonymous class.
	private OnItemClickListener mMessageClickedHandler = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
				long arg3) {
			System.out.println(parents);
			System.out.println(arg1);
			System.out.println(arg2);
			System.out.println(arg3);

			ArrayList<MoviePersons> moviePersons = movie.getMoviePersons();
			MoviePersons clicked_person = moviePersons.get(arg2);
			System.out.println(clicked_person.getName());
			Integer this_person_id = (Integer) clicked_person.getId();
			System.out.println(this_person_id);

			Intent intent = new Intent(parents.getContext(), PersonInfo.class);
			String[] intent_messages = { this_person_id.toString(), server };
			intent.putExtra(PERSON_ID_MESSAGE, intent_messages);
			startActivity(intent);

		}
	};

	// Create a message handling object as an anonymous class.
	private OnItemClickListener mImageClickedHandler = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
				long arg3) {
			// System.out.println(parents);
			// System.out.println(arg1);
			// System.out.println(arg2);
			// System.out.println(arg3);

			 Intent intent = new Intent(parents.getContext(),
			 ImagesViewFull.class);
			 ArrayList<apiImageUrls> image_urls = movie.images;
			 intent.putExtra(IMAGE_URLs_MESSAGE, image_urls);
			 intent.putExtra(IMAGE_URLs_ID, arg2);
			 startActivity(intent);
//			 System.out.println(movie.images.get(arg2).getImageUrlActual());
//			try{
//			Intent i = new Intent(Intent.ACTION_VIEW,
//					Uri.parse(ServerVariables.server
//							+ movie.images.get(arg2).getImageUrlActual()));
//			startActivity(i);
//			}catch(Exception e){
//				System.out.println("error while poping out");
//			}
		}
	};
	
	// Create a message handling object as an anonymous class.
	private OnItemClickListener mTrailerImageClickedHandler = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
				long arg3) {
			 System.out.println(parents);
			 System.out.println(arg1);
			 System.out.println(arg2);
			 System.out.println(arg3);

			// Intent intent = new Intent(parents.getContext(),
			// ImagesViewFull.class);
			// ArrayList<apiImageUrls> image_urls = movie.images;
			// intent.putExtra(IMAGE_URLs_MESSAGE, image_urls);
			// intent.putExtra(IMAGE_URLs_ID, arg2);
			// startActivity(intent);
			// System.out.println(movie.images.get(arg2).getImageUrlActual());
			 try{
			Intent i = new Intent(Intent.ACTION_VIEW,
					Uri.parse(ServerVariables.youtube_video_url_start+movie.getTrailers().get(arg2)));
			startActivity(i);
			 }catch(Exception e){
				 System.out.println("error while poping out image");
			 }
		}
	};

	// Test FOR ViewPager adapter
	private class SampleAdapter extends PagerAdapter {
		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			if (position == 0) {
				try {

					View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_zero, container,
							false);
// --------- Floating Menu
					View menu_page = getLayoutInflater().inflate(R.layout.movie_info_layout_item_all, container,false);
					TextView d1 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc1));
					d1.setText("Trailers");
					SpannableString menu_underline = new SpannableString("Movie Banner");
					menu_underline.setSpan(new UnderlineSpan(), 0, menu_underline.length(), 0);
					TextView d2 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc2));
					d2.setText(menu_underline);
					TextView d3 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc3));
					d3.setText("Movie Info");
					LinearLayout llzero = (LinearLayout) page.findViewById(R.id.movie_info_ll_zero);
					llzero.addView(menu_page, 0);
					d3.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(position+1);
					    }
					});
					d1.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(5);
					    }
					});
// Floating Menu Ends
					
					int blue = position * 25;
					ImageView img = (ImageView) page.findViewById(R.id.imgIcon);
					String backgroundImageUrl = movie.getBackgroundImageUrl();

					imageLoader.DisplayImage(backgroundImageUrl, img);

					page.setBackgroundColor(Color.argb(255, 0, 0, blue));
					container.addView(page);
					System.out.println(backgroundImageUrl);
					return (page);

				} catch (Exception e) {
					System.out.println("error @displaying0: " + e);
					return null;
				}
			} else if (position == 1) {
				try {

					View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_one, container,
							false);
// --------- Floating Menu
					View menu_page = getLayoutInflater().inflate(R.layout.movie_info_layout_item_all, container,false);
					TextView d1 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc1));
					d1.setText("Movie Banner");
					SpannableString menu_underline = new SpannableString("Movie Info");
					menu_underline.setSpan(new UnderlineSpan(), 0, menu_underline.length(), 0);
					TextView d2 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc2));
					d2.setText(menu_underline);
					TextView d3 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc3));
					d3.setText("Movie Cast");
					LinearLayout llzero = (LinearLayout) page.findViewById(R.id.movie_info_ll_one);
					llzero.addView(menu_page, 0);
					d3.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(position+1);
					    }
					});
					d1.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(position-1);
					    }
					});
// Floating Menu Ends
					TextView tv = (TextView) page.findViewById(R.id.txtTitle);
					tv.setText(Html.fromHtml("<h2>Movie Info</h2>"));
//					tv.append("\n");
					tv.append(Html.fromHtml("<h4>Name: "+ movie.getTitle()+"</h4>"));
					System.out.println("In pager");
					System.out.println(movie.getTitle());
					System.out.println(movie.getStatus());
//					tv.append("\n");
//					date
					Calendar c = new GregorianCalendar();
					try {
						c.add(Calendar.DATE, movie.getDays_left());
					} catch (Exception e) {
						System.out
								.println("Error while adding days to date ERROR: "
										+ e);
					}
					Date d = new Date(c.getTimeInMillis());
					String sdf = new SimpleDateFormat("dd-MMMM-yyyy",
							Locale.getDefault()).format(d);
//					dateend
					if(movie.getStatus().equals("F")){
						if(movie.getDays_left()>-1000){
							tv.append(Html.fromHtml("<h4>Status: Not In Theaters. Released "+Math.abs(movie.getDays_left()) +" days back on "+sdf+"</h4>"));
						}else{
							tv.append(Html.fromHtml("<h4>Status: Not In Theaters. Released on "+sdf+"</h4>"));
						}
					}else if(movie.getStatus().equals("T")){
						if(movie.getDays_left()>-300){
							tv.append(Html.fromHtml("<h4>Status: In Theaters. Released "+Math.abs(movie.getDays_left())+" days back on "+sdf+"</h4>"));
						}else{
							tv.append(Html.fromHtml("<h4>Status: In Theaters. Released on "+sdf+"</h4>"));
						}
					}else if(movie.getStatus().equals("D")){
						if(movie.getDays_left()<300 && movie.getDays_left()>=0){
							tv.append(Html.fromHtml("<h4>Status: Going To Release In "+Math.abs(movie.getDays_left())+" days on "+sdf+"</h4>"));
						}else{
							tv.append(Html.fromHtml("<h4>Status: Going To Release - Date To Be Announced"+"</h4>"));
						}
					}
					tv.append(Html.fromHtml("<h2>Movie Plot</h2>"));
					tv.append(Html.fromHtml(movie.getPlot()));
					tv.append("\n");
					tv.append("\n");
					tv.append(Html.fromHtml("<h3>Additional Info</h3>"));
					tv.append(Html.fromHtml(movie.getInfo()));
					container.addView(page);

					return (page);

				} catch (Exception e) {
					System.out.println("error @displaying1: " + e);
					return null;
				}
			} else if (position == 2) {

				try {
					View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_two, container,
							false);
		
// --------- Floating Menu
					View menu_page = getLayoutInflater().inflate(R.layout.movie_info_layout_item_all, container,false);
					TextView d1 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc1));
					d1.setText("Movie Info");
					SpannableString menu_underline = new SpannableString("Movie Cast");
					menu_underline.setSpan(new UnderlineSpan(), 0, menu_underline.length(), 0);
					TextView d2 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc2));
					d2.setText(menu_underline);
					TextView d3 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc3));
					d3.setText("Movie Images");
					RelativeLayout llzero = (RelativeLayout) page.findViewById(R.id.movie_info_rl_two);
					llzero.addView(menu_page, 0);
					d3.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	System.out.println("clicked3");
					    	pager.setCurrentItem(position+1);
					    }
					});
					d1.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	System.out.println("clicked1");
					    	pager.setCurrentItem(position-1);
					    }
					});
// Floating Menu Ends
					
					
					ArrayList<MoviePersons> moviePersons = movie
							.getMoviePersons();

					container.addView(page);
					try {

						MovieInfoPersonsAdapter adapter = new MovieInfoPersonsAdapter(page.getContext(),R.layout.movie_info_cast_adapter, moviePersons);
						// GridView gridView =
						// (GridView)findViewById(R.id.GridView1);
						ListView listView = (ListView) findViewById(R.id.personslist);
						// gridView.setAdapter(adapter);
						listView.setAdapter(adapter);
						listView.setOnItemClickListener(mMessageClickedHandler);
					} catch (Exception e) {
						System.out
								.println("error while setting up adapter" + e);
					}

					return (page);

				} catch (Exception e) {
					System.out.println("error @displaying2: " + e);
					return null;
				}
			} else if (position == 4) {
				try {

					View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_one, container,
							false);

// --------- Floating Menu
					View menu_page = getLayoutInflater().inflate(R.layout.movie_info_layout_item_all, container,false);
					TextView d1 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc1));
					d1.setText("Movie Images");
					SpannableString menu_underline = new SpannableString("Movie Rating");
					menu_underline.setSpan(new UnderlineSpan(), 0, menu_underline.length(), 0);
					TextView d2 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc2));
					d2.setText(menu_underline);
					TextView d3 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc3));
					d3.setText("Movie Trailers");
					LinearLayout llzero = (LinearLayout) page.findViewById(R.id.movie_info_ll_one);
					llzero.addView(menu_page, 0);
					d3.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(position+1);
					    }
					});
					d1.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(position-1);
					    }
					});
// Floating Menu Ends
					
					TextView tv = (TextView) page.findViewById(R.id.txtTitle);

					tv.setText("Rating Section: Comming Soon..");

					container.addView(page);

					return (page);

				} catch (Exception e) {
					System.out.println("error @displaying4: " + e);
					return null;
				}
			} else if (position == 3) {
				try {

					final View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_four, container,
							false);
// --------- Floating Menu
					View menu_page = getLayoutInflater().inflate(R.layout.movie_info_layout_item_all, container,false);
					TextView d1 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc1));
					d1.setText("Movie Cast");
					SpannableString menu_underline = new SpannableString("Movie Images");
					menu_underline.setSpan(new UnderlineSpan(), 0, menu_underline.length(), 0);
					TextView d2 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc2));
					d2.setText(menu_underline);
					TextView d3 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc3));
					d3.setText("Movie Rating");
					LinearLayout llzero = (LinearLayout) page.findViewById(R.id.movie_info_ll_four);
					llzero.addView(menu_page, 0);
					d3.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(position+1);
					    }
					});
					d1.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(position-1);
					    }
					});
// Floating Menu Ends
					
					// TextView tv = (TextView)
					// page.findViewById(R.id.txtTitle);

					// tv.setText("Images Section: Comming Soon..");

					container.addView(page);
					// ArrayList<String> urls = null;
					// overriding my geturl class and replace the postresult
					// method with myown
					class datatest extends getURLdata {
						@Override
						public void postresult(Object result2) {
							String[] tempurls = null;
							try {
								addtoCacheFile((String) result2,
										fileMovieImages);
							} catch (Exception e) {
								System.out
										.println("unable to add urls to files");
							}
							try {
								ArrayList<apiImageUrls> arrayurls = parseimages(result2);
								String[] urls = new String[arrayurls.size()];
								for (int x = 0; x < arrayurls.size(); x++) {
									apiImageUrls currentImageUrl = arrayurls
											.get(x);
									urls[x] = (server + currentImageUrl
											.getImageUrlSmall());
								}
								tempurls = urls;
							} catch (Exception e) {
								System.out
										.println("error in forloop position 4 "
												+ e);
							}

							try {
								MainImageAdapter adapter = new MainImageAdapter(
										page.getContext(),
										R.layout.main_layout_back, tempurls);
								// GridView gridView =
								// (GridView)findViewById(R.id.GridView1);
								GridView gridview = (GridView) findViewById(R.id.movie_info_layout_four_gridview1);
								// gridView.setAdapter(adapter);
								gridview.setAdapter(adapter);
								gridview.setOnItemClickListener(mImageClickedHandler);
							} catch (Exception e) {
								System.out
										.println("error while setting up adapter"
												+ e);
							}
						}
					}
					
					Boolean cacheBoolean = false;
					String res1 = new String();
					// ------------ LOTS OF REPEAT CODE INCOMING HERE
					if (fileMovieImages.exists()) {
						FileReader fr = new FileReader(fileMovieImages);
						if (fr.read() == -1) {
							System.out.println("EMPTY");
							cacheBoolean = false;
						} else {
							try {
								res1 = readFile(fileMovieImages
										.getAbsolutePath());
								cacheBoolean = true;
							} catch (IOException e) {
								System.out
										.println("error while reading cache file after its not empty "
												+ e);
								cacheBoolean = false;
							}
						}

					} else {
						System.out.println("DOES NOT EXISTS");
						cacheBoolean = false;
					}
					if (!cacheBoolean || update_info) {
						System.out.println("in network from menu updateimages");
						datatest geturldata = new datatest();
						geturldata.getData(image_server);
					} else {
						//REPEAT CODE STARTS HERE GETTING DATA FROM CACHE AND DISPLAYING
						String[] tempurls = null;
						try {
						} catch (Exception e) {
							System.out.println("unable to add urls to files");
						}
						try {
							ArrayList<apiImageUrls> arrayurls = parseimages(res1);
							String[] urls = new String[arrayurls.size()];
							for (int x = 0; x < arrayurls.size(); x++) {
								apiImageUrls currentImageUrl = arrayurls.get(x);
								urls[x] = (server + currentImageUrl
										.getImageUrlSmall());
							}
							tempurls = urls;
						} catch (Exception e) {
							System.out.println("error in forloop position 4 "
									+ e);
						}

						try {
							MainImageAdapter adapter = new MainImageAdapter(
									page.getContext(),
									R.layout.main_layout_back, tempurls);
							// GridView gridView =
							// (GridView)findViewById(R.id.GridView1);
							GridView gridview = (GridView) findViewById(R.id.movie_info_layout_four_gridview1);
							// gridView.setAdapter(adapter);
							gridview.setAdapter(adapter);
							gridview.setOnItemClickListener(mImageClickedHandler);
						} catch (Exception e) {
							System.out.println("error while setting up adapter"
									+ e);
						}
						// REPEAT CODE ENDS HERE
					}
					return (page);

				} catch (Exception e) {
					System.out.println("error @displaying1: " + e);
					return null;
				}
			} else if(position == 5){
				try {

					View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_five, container,
							false);

// --------- Floating Menu
					View menu_page = getLayoutInflater().inflate(R.layout.movie_info_layout_item_all, container,false);
					TextView d1 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc1));
					d1.setText("Movie Rating");
					SpannableString menu_underline = new SpannableString("Movie Trailers");
					menu_underline.setSpan(new UnderlineSpan(), 0, menu_underline.length(), 0);
					TextView d2 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc2));
					d2.setText(menu_underline);
					TextView d3 = ((TextView)menu_page.findViewById(R.id.movie_info_layout_desc3));
					d3.setText("Movie Banner");
					LinearLayout llzero = (LinearLayout) page.findViewById(R.id.movie_info_ll_five);
					llzero.addView(menu_page, 0);
					d3.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(0);
					    }
					});
					d1.setOnClickListener(new View.OnClickListener() {
					    public void onClick(View v) {
					    	pager.setCurrentItem(position-1);
					    }
					});
// Floating Menu Ends
					
//					TextView tv = (TextView) page.findViewById(R.id.txtTitle);
//
//					tv.setText("Rating Section: Comming Soon..");
//
//					container.addView(page);
//
//					return (page);
					container.addView(page);
					String tempurls[] = null;
					
					try {
						ArrayList<String> arrayurls = movie.getTrailers();
						String[] urls = new String[arrayurls.size()];
						for (int x = 0; x < arrayurls.size(); x++) {
							String currentImageUrl = arrayurls.get(x);
							urls[x] = ServerVariables.youtube_image_url_start + currentImageUrl+ServerVariables.youtube_image_url_end;
						}
						tempurls = urls;
					} catch (Exception e) {
						System.out.println("error in forloop position 5 "+ e);
					}
					
					try {
						MainImageAdapter adapter2 = new MainImageAdapter(page.getContext(),R.layout.main_layout_back, tempurls);
						// GridView gridView =
						// (GridView)findViewById(R.id.GridView1);
						GridView gridview2 = (GridView) findViewById(R.id.movie_info_layout_five_gridview1);
						// gridView.setAdapter(adapter);
						gridview2.setAdapter(adapter2);
						gridview2.setOnItemClickListener(mTrailerImageClickedHandler);
					} catch (Exception e) {
						System.out.println("error while setting up adapter"+ e);
					}
					
					return page;

				} catch (Exception e) {
					System.out.println("error @displaying5: " + e);
					return null;
				}
			}
			else
				return null;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return (6);
		}

		@Override
		public float getPageWidth(int position) {
			return (1f);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return (view == object);
		}
	}

	public void addtoCacheFile(String string, File f) {
		System.out.println(f.getName());
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					f.getAbsolutePath(), false)));
		} catch (IOException e) {
			System.out.println(e);
		}
		if (out != null)
			out.println(string);
		if (out != null)
			out.close();

	}
}
