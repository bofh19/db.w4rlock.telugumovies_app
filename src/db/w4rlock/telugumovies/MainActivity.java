package db.w4rlock.telugumovies;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import db.w4rlock.telugumovies.Adapters.MainImageAdapter;
import db.w4rlock.telugumovies.utils.FileCache;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class MainActivity extends Activity {	
	private MainImageAdapter adapter;
	private GridView gridView;
	public ArrayList<String> url_data;
	public ArrayList<Integer> movie_id;
	public String server = ServerVariables.server;
	public String server_url = ServerVariables.releasing_images_url;
	
	public TextView loading = null;
	public static final String DEBUG_TAG = "SERVER RESPONSE: ";
	public final static String MOVIE_ID_MESSAGE = "db.w4rlock.telugumovies.MESSAGE";
	public final static String UPDATE_ONLINE_MESSAGE = "db.w4rlock.telugumovies.UPDATE_ONLINE_MESSAGE";
	FileCache fileCache;
	public File f=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        url_data = new ArrayList<String>();
        movie_id = new ArrayList<Integer>();
        loading = (TextView)findViewById(R.id.loading);
        fileCache = new FileCache(getApplicationContext());
        f=fileCache.getFile(server_url);
        Boolean cachecheck=false;
        Boolean yes_intent = false;
        try{
            Intent intent = getIntent();
            String[] intent_messages = intent.getStringArrayExtra(MainActivity.UPDATE_ONLINE_MESSAGE);
            String intent_msg0 = intent_messages[0];
            int check = Integer.parseInt(intent_msg0);
            if(check == 1){
            	yes_intent = true;
            }
            }catch(Exception e){
            	System.out.println("no intent value started from start");
        }
        if(!yes_intent){
	        try {
				cachecheck = runfromfile(f);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				cachecheck = false;
				e1.printStackTrace();
			}
        }
        if(!cachecheck){
        	System.out.println("in network "+!cachecheck);
	        ConnectivityManager connMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	//        System.out.println("asdf");
	      	if(networkInfo != null && networkInfo.isConnected()){
	      		Log.d("network", "loading data");
	    		try{
	    			Log.d("network","connecting");
	    		new DownloadImagesUrls().execute(server_url);
	    		}catch(Exception e){
	    			System.out.println(e);
	    			Log.d("network","error on getting data");
	    		}
	    	}else {
	    		Log.d("network","no network connection avilable");
	    	}
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent intent = new Intent(this,HomePage.class);
	    	startActivity(intent);
			return true;
		case R.id.menu_update:
			Intent intent1 = getIntent();
			String[] intent_messages = {"1"};
	    	intent1.putExtra(UPDATE_ONLINE_MESSAGE, intent_messages);
			finish();
			startActivity(intent1);
			return true;
		case R.id.menu_reload:
			Intent intent11=getIntent();
			String[] intent_messages1 = {"0"};
	    	intent11.putExtra(UPDATE_ONLINE_MESSAGE, intent_messages1);
			finish();
			startActivity(intent11);
			return true;
		case R.id.grid_default_search:

			Intent intent12 = new Intent(this,SearchActivity.class);
			startActivity(intent12);
		default:
			return super.onOptionsItemSelected(item);
		}
    }
 
//    public void goHomePage(View view){
//    	Intent intent = new Intent(this,HomePage.class);
//    	startActivity(intent);
//    }
    private static String readFile(String path) throws IOException {
    	  FileInputStream stream = new FileInputStream(new File(path));
    	  try {
    	    FileChannel fc = stream.getChannel();
    	    MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
    	    /* Instead of using default, pass in a decoder. */
    	    return Charset.defaultCharset().decode(bb).toString();
    	  }
    	  finally {
    	    stream.close();
    	  }
    	}
    public boolean runfromfile(File f) throws IOException{
    	String res = "";
        if (f.exists()) {
            FileReader fr = new FileReader(f);
            if (fr.read() == -1) {
                System.out.println("EMPTY");
                return false;
            } else {
                try{
                	res = readFile(f.getAbsolutePath());
                }catch(IOException e){
                	System.out.println("error while reading cache file after its not empty "+e);
                	return false;
                }
            }
        } else {
            System.out.println("DOES NOT EXISTS");
            return false;
        }
    	try {
			JSONArray entries = new JSONArray(res);
			for(int i=0;i<entries.length();i++){
				 JSONObject thisobj = (JSONObject)entries.get(i);
				 url_data.add(server+(String)thisobj.get("banner_image_small"));
				 movie_id.add((Integer)thisobj.get("id"));
//				 System.out.println("data from file cache "+thisobj.get("id"));
			}
		}catch(Exception e){
			System.out.println("error while parsing movies from cachefile "+e);
			System.out.println(e);
			return false;
		}
		goadapter();
		return true;
    }
   
    public void postresult(Object result){
    	String res = (String)result;
    	//System.out.println(res);
		try {
			JSONArray entries = new JSONArray(res);
			for(int i=0;i<entries.length();i++){
				 JSONObject thisobj = (JSONObject)entries.get(i);
				 url_data.add(server+(String)thisobj.get("banner_image_small"));
				 movie_id.add((Integer)thisobj.get("id"));
			}
		}catch(Exception e){
			System.out.println("error while parsing movies "+e);
			System.out.println(e);
		}
		goadapter();
    }
    public void goadapter(){
    	String[] url_data_array = url_data.toArray(new String[url_data.size()]);
    	try{
    	adapter = new MainImageAdapter(this, R.layout.main_layout_back,url_data_array);
    	gridView = (GridView)findViewById(R.id.GridView1);

    	// to get header just inflate the header and then use 
    	// listview.addheader command .. <just so i wont forget>
    	
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(mMessageClickedHandler);
    	}catch (Exception e){
    		System.out.println("error on goadapter"+e);
    	}
    	
    	
    	loading.setVisibility(View.GONE);
    	
    	try{
    		View loadingbar = (View)findViewById(R.id.progress_large);
    		loadingbar.setVisibility(View.GONE);
    	}catch(Exception e){
    		System.out.println("Error while removin loading bar "+e);
    	}
    }
    
    // Create a message handling object as an anonymous class.
    private OnItemClickListener mMessageClickedHandler = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
				long arg3) {
//			System.out.println(parents);
//			System.out.println(arg1);
//			System.out.println(arg2);
//			System.out.println(arg3);
			Integer this_movie_id = movie_id.get(arg2);
			System.out.println(this_movie_id);
			
			Intent intent = new Intent(parents.getContext(),MovieInfo.class);
	    	String[] intent_messages = {this_movie_id.toString(),server};
	    	intent.putExtra(MOVIE_ID_MESSAGE, intent_messages);
	    	startActivity(intent);
			
		}
    };
    
 
    private class DownloadImagesUrls extends AsyncTask<Object, Object, Object> {

		@Override
		protected Object doInBackground(Object... arg0) {
			String data = (String)arg0[0];
			try {
				return downloadUrl(data);
			} catch (IOException e) {
				return "unable to get the data sry :(";
			}
		}
		
		@Override
        protected void onPostExecute(Object result) {
			if(result == null || result == " " || result == ""){
				loading.setText("result is broken are you online?");
			}else{
			postresult(result);
			}
		}
		
		private String downloadUrl(String myurl) throws IOException {
		    InputStream is = null;
		        
		    try {
		    	//System.out.println(myurl);
		        URL url = new URL(myurl);
		        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		        conn.setReadTimeout(10000 /* milliseconds */);
		        conn.setConnectTimeout(15000 /* milliseconds */);
		        conn.setRequestMethod("GET");
		        conn.setDoInput(true);
		        // Starts the query
		        try{
		        conn.connect();
		        }catch(Exception e){
		        	System.out.println("Error while connection "+e);
		        	return" ";
		        }
		        System.out.println("connection request above");
		        int response = conn.getResponseCode();
		        Log.d(DEBUG_TAG, " "+response);
		        try{
		        is = conn.getInputStream();
		        }catch(Exception e){
		        	System.out.println("error while getting inputstream in do background "+e);
		        	return " ";
		        }
		        String contentAsString = readAll(is);
		       
		        return contentAsString;
		        
		    } catch (Exception e){
		    	System.out.println("error on inputstream "+e);
		    	return " ";
		    }
		    finally {
		        if (is != null) {
		            is.close();
		        }else {
		        	return null;
		        }
		    }
		}
		
	   public String readAll(InputStream stream) throws IOException, UnsupportedEncodingException {
		   StringBuilder sb = new StringBuilder();
		   String s;
		   try{
		   Reader reader = new InputStreamReader(stream,"UTF-8");
		   BufferedReader buf = new BufferedReader(reader);
		   while(true){
		    	s=buf.readLine();
		    	if(s==null || s.length()==0)
		    		break;
		    	sb.append(s);
		    }	
		   	addtoCacheFile(sb.toString(),f);
		   return sb.toString();
		   }catch(Exception e){
			   System.out.println("error on readall ie catching input stream");
			   return " ";
		   }
	   }
   }


	public void addtoCacheFile(String string, File f) {
		PrintWriter out = null;
		   try {
			    out = new PrintWriter(new BufferedWriter(new FileWriter(f.getAbsolutePath(), false)));
		   } catch (IOException e) {
			    System.out.println(e);
			}
		   if (out != null)
	    		out.println(string);
		   if (out !=null)
		    	out.close();
	}
}


