package db.w4rlock.telugumovies;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class MainImageAdapter extends ArrayAdapter<String>{
	Context context;
	int layoutResourceId;
	String data[] = null;
	public ImageLoader imageLoader;
	public MainImageAdapter(Context context, int layoutResourceId,String[] data) {
		super(context, layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		imageLoader = new ImageLoader(context.getApplicationContext());
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		ImageView img = null;
//		System.out.println(position);
		if(row == null){
			
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent,false);
			img = (ImageView)row.findViewById(R.id.grid_item_image);
//			System.out.println(img);

			String this_url = data[position];
			//System.out.println(this_url);	
			try{
				imageLoader.DisplayImage(this_url, img);
			}catch(Exception e){
				System.out.println("error while displaying image@ "+position+e);
			}
			
		}else {
//			img = (ImageView)row.getTag();
		}
		
		
		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.slide_out_right);
        row.startAnimation(animation);
		return row;
		
	}

}