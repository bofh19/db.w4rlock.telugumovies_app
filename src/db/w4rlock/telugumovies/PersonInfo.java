package db.w4rlock.telugumovies;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import db.w4rlock.telugumovies.Adapters.MainImageAdapter;
import db.w4rlock.telugumovies.Adapters.PersonInfoMoviesAdapter;
import db.w4rlock.telugumovies.Models.Movie;
import db.w4rlock.telugumovies.Models.MoviePersons;
import db.w4rlock.telugumovies.Models.apiImageUrls;
import db.w4rlock.telugumovies.utils.FileCache;
import db.w4rlock.telugumovies.utils.ImageLoader;
import db.w4rlock.telugumovies.utils.getURLdata;

public class PersonInfo extends Activity {

	public String person_api_server = "/mobile/v1/api/p";
	public final static String image_api_server = "/mobile/v1/api/p/images";
	public final static String PERSON_ID_MESSAGE = "db.w4rlock.telugumovies.MESSAGE";
	public final static String MOVIE_ID_MESSAGE = "db.w4rlock.telugumovies.MESSAGE";
	public final static String IMAGE_URLs_MESSAGE = "db.w4rlock.telugumovies.IMAGE_URLs_MESSAGE";
	public final static String UPDATE_ONLINE_MESSAGE = "db.w4rlock.telugumovies.UPDATE_ONLINE_MESSAGE";
	public final static String IMAGE_URLs_ID = "db.w4rlock.telugumovies.IMAGE_URLs_ID";
	public String[] intent_messages = null;
	public String server = "";
	public TextView person_title;
	public String image_server = null;
	public MoviePersons person;

	public static final String DEBUG_TAG = "server reponse";
	public ImageLoader imageLoader;
	public ViewPager pager = null;
	FileCache fileCache;
	public File filePersonInfo = null;
	public File filePersonImages = null;
	public Integer this_movie_id = null;
	public String api_server;
	public Boolean update_info = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_person_info);
		person_title = (TextView) findViewById(R.id.person_title);
		Intent intent = getIntent();
		intent_messages = intent.getStringArrayExtra(PERSON_ID_MESSAGE);
		Integer this_person_id = Integer.parseInt(intent_messages[0]);
		server = new String(ServerVariables.server);
		api_server = server + person_api_server + "/" + this_person_id
				+ "/";
		image_server = server + image_api_server + "/" + this_person_id + "/";
		imageLoader = new ImageLoader(this.getApplicationContext());
		person = new MoviePersons();
		Boolean filechecks = false;
		Boolean cachecheck = false;
		this.setTitle("movie title");
		try {
			fileCache = new FileCache(getApplicationContext());
			filePersonInfo = fileCache.getFile(api_server);
			filePersonImages = fileCache.getFile(image_server);
			filechecks = true;
		} catch (Exception e) {
			System.out.println(e);
		}
		if (filechecks) {
			try {
				cachecheck = runfromfilePersonInfo(filePersonInfo);
			} catch (IOException e) {
				System.out.println("runfromfileerror1");
				e.printStackTrace();
			}
		}
		
		if(!cachecheck){
			ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				person_title.setText("loading data...");
				try {
					person_title.setText("contacting server");
					new DownloadWebpageText().execute(api_server);
				} catch (Exception e) {
					System.out.println(e);
					person_title.setText("Error On Getting Data");
				}
			} else {
				person_title.setText("no network connection avilable");
			}
		}
	}
	
	private static String readFile(String path) throws IOException {
		FileInputStream stream = new FileInputStream(new File(path));
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}
	
	private Boolean runfromfilePersonInfo(File fileMovieInfo2)
			throws IOException {
		String res1 = "";
		if (fileMovieInfo2.exists()) {
			FileReader fr = new FileReader(fileMovieInfo2);
			if (fr.read() == -1) {
				System.out.println("EMPTY");
				return false;
			} else {
				try {
					res1 = readFile(fileMovieInfo2.getAbsolutePath());
				} catch (IOException e) {
					System.out
							.println("error while reading cache file after its not empty "
									+ e);
					return false;
				}
			}
		} else {
			System.out.println("DOES NOT EXISTS");
			return false;
		}
		postresult(res1);
		return true;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_person_info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_person_info:
			try {
				pager.setCurrentItem(1);
			} catch (Exception e) {
				System.out.println("Error while changing page: " + e);
			}
			return true;
		case R.id.menu_person_movies:
			try {
				pager.setCurrentItem(2);
			} catch (Exception e) {
				System.out.println("Error while changing page: " + e);
			}
			return true;
		case R.id.menu_person_images:
			try {
				pager.setCurrentItem(3);
			} catch (Exception e) {
				System.out.println("Error while changing page: " + e);
			}
			return true;
			// main settings
		case R.id.menu_main_page:
			try {
				Intent intent = new Intent(this, MainActivity.class);
				startActivity(intent);
			} catch (Exception e) {
				System.out.println("Error while changing activity: " + e);
			}
			return true;
		case R.id.menu_index:
			try {
				Intent intent = new Intent(this, HomePage.class);
				startActivity(intent);
			} catch (Exception e) {
				System.out.println("Error while changing acitivty: " + e);
			}
			return true;
		case R.id.menu_update:
			updateinfo();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
private void updateinfo() {
		System.out.println("in network from menu");
		update_info = true;
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		
		if (networkInfo != null && networkInfo.isConnected()) {
			
			person_title.setText("loading data...");
			try {
				
				person_title.setText("contacting server");
				
				new DownloadWebpageText().execute(api_server);
				System.out.println(api_server);
			} catch (Exception e) {
				System.out.println(e);
				person_title.setText("Error On Getting Data");
			}
		} else {
			person_title.setText("no network connection avilable");
		}
		
	}
	
	public void postresult(Object result) {
		JSONObject thisobj = new JSONObject();
		String res = (String) result;
		person_title.setText(res);

		try {
			View loadingbar = (View) findViewById(R.id.progress_large);
			loadingbar.setVisibility(View.GONE);
			person_title.setVisibility(View.GONE);
		} catch (Exception e) {
			System.out.println("Error while removin loading bar: " + e);
		}

		try {
			thisobj = new JSONObject(res);
		} catch (Exception e) {
			System.out.println("error while casting to object: " + e);
		}
		try {
			person.setName((String) thisobj.get("name"));
			System.out.println(thisobj.get("name"));
			person.setId(thisobj.getInt("id"));
			person.setBio(thisobj.getString("bio"));
			person.setProfileImageUrl(server
					+ thisobj.getString("profile_image"));
			person.setProfileImageUrlSmall(server
					+ thisobj.getString("profile_image_small"));
			this.setTitle(person.getName());
		} catch (Exception e) {
			System.out.println("error while getting data from obj: " + e);
		}
		try {
			JSONArray moviesArray = new JSONArray();
			moviesArray = (JSONArray) thisobj.get("movies");
			ArrayList<Movie> personsMovies = new ArrayList<Movie>();
			for (int x = 0; x < moviesArray.length(); x++) {
				Movie currentMovie = new Movie();
				JSONObject currentMovieJsonObject = moviesArray
						.getJSONObject(x);

				int id = currentMovieJsonObject.getInt("id");
				String name = currentMovieJsonObject.getString("name");
				String banner_image_url = currentMovieJsonObject
						.getString("banner_image");
				banner_image_url = server + banner_image_url;
				String banner_image_small_url = currentMovieJsonObject
						.getString("banner_image_small");
				banner_image_small_url = server + banner_image_small_url;

				currentMovie.setMovie_id(id);
				currentMovie.setTitle(name);
				currentMovie.setIcon(banner_image_url);
				currentMovie.setIcon_small(banner_image_small_url);

				personsMovies.add(currentMovie);
			}
			person.setMovies(personsMovies);
			pager=(ViewPager)findViewById(R.id.viewpager_personinfo);
            pager.setAdapter(new SampleAdapter());
            pager.setOffscreenPageLimit(4);
		} catch (Exception e) {
			System.out.println("error while getting movies: " + e);
		}
	}

	public ArrayList<apiImageUrls> parseimages(Object result2) {
		String res = (String) result2;
		JSONObject thisobj = new JSONObject();
		try {
			thisobj = new JSONObject(res);
		} catch (Exception e) {
			System.out.println("error while parsing images " + e);
		}
		try {
			JSONArray iarray = thisobj.getJSONArray("images");
			ArrayList<apiImageUrls> imageurlslist = new ArrayList<apiImageUrls>();
			for (int x = 0; x < iarray.length(); x++) {
				apiImageUrls currentimage = new apiImageUrls();
				JSONObject currentobj = iarray.getJSONObject(x);
				currentimage.setImageUrlSmall(currentobj.getString("small"));
				currentimage.setImageUrlActual(currentobj.getString("actual"));
				// System.out.println(currentobj.getString("actual"));
				imageurlslist.add(currentimage);
			}
			person.setImages(imageurlslist);
			return imageurlslist;
		} catch (Exception e) {
			System.out.println("error while getting images array " + e);
		}
		return null;
	}

	// Create a message handling object as an anonymous class.
	private OnItemClickListener mMessageClickedHandler = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
				long arg3) {
			System.out.println(parents);
			System.out.println(arg1);
			System.out.println(arg2);
			System.out.println(arg3);

			ArrayList<Movie> movies = person.getMovies();
			Movie clicked_movie = movies.get(arg2);
			System.out.println(clicked_movie.title);
			Integer this_movie_id = (Integer) clicked_movie.movie_id;

			Intent intent = new Intent(parents.getContext(), MovieInfo.class);
			String[] intent_messages = { this_movie_id.toString(), server };
			intent.putExtra(MOVIE_ID_MESSAGE, intent_messages);
			startActivity(intent);

		}
	};
	private OnItemClickListener mImageClickedHandler = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
				long arg3) {
			// System.out.println(parents);
			// System.out.println(arg1);
			// System.out.println(arg2);
			// System.out.println(arg3);

			Intent intent = new Intent(parents.getContext(),
					ImagesViewFull.class);
			ArrayList<apiImageUrls> image_urls = person.images;
			intent.putExtra(IMAGE_URLs_MESSAGE, image_urls);
			intent.putExtra(IMAGE_URLs_ID, arg2);
			startActivity(intent);
//			System.out.println(movie.images.get(arg2).getImageUrlActual());
			
//			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(ServerVariables.server+person.images.get(arg2).getImageUrlActual()));
//			startActivity(i);
		}
	};

	private class DownloadWebpageText extends AsyncTask<Object, Object, Object> {

		@Override
		protected Object doInBackground(Object... params) {
			String data = (String) params[0];
			try {
				return downloadUrl(data);
			} catch (IOException e) {
				return "unable to get the data sry :(";
			}
		}

		@Override
		protected void onPostExecute(Object result) {
			if (result == null || result == " " || result == "") {
				person_title.setText("result is broken are you online ?");
			} else {
				postresult(result);
				// System.out.println("got data on postexceute");
			}
		}

		private String downloadUrl(String myurl) throws IOException {
			InputStream is = null;

			try {
				System.out.println(myurl);
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);
				// Starts the query
				try {
					conn.connect();
				} catch (Exception e) {
					System.out.println("Error while connection: " + e);
					return " ";
				}
				System.out.println("connection request above");
				int response = conn.getResponseCode();
				Log.d(DEBUG_TAG, " " + response);
				try {
					is = conn.getInputStream();
				} catch (Exception e) {
					System.out
							.println("error while getting inputstream in do background: "
									+ e);
					return " ";
				}
				String contentAsString = readAll(is);
				return contentAsString;

			} catch (Exception e) {
				System.out.println(e);
				System.out.println("x");
				return " ";
			} finally {
				if (is != null) {
					is.close();
				} else {
					return null;
				}
			}
		}

		public String readAll(InputStream stream) throws IOException,
				UnsupportedEncodingException {
			StringBuilder sb = new StringBuilder();
			String s;
			try {
				Reader reader = new InputStreamReader(stream, "UTF-8");
				BufferedReader buf = new BufferedReader(reader);
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);
				}
				try {
					addtoCacheFile(sb.toString(), filePersonInfo);
				} catch (Exception e) {
					System.out.println("error on read all"+e);
				}
				return sb.toString();
			} catch (Exception e) {
				System.out.println("error on readall ie catching input stream");
				return " ";
			}
		}
	}

	// Test FOR ViewPager adapter
	private class SampleAdapter extends PagerAdapter {

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			if (position == 0) {
				try {

					View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_zero, container,
							false);

					int blue = position * 25;
					ImageView img = (ImageView) page.findViewById(R.id.imgIcon);
					String backgroundImageUrl = person.getProfileImageUrl();
					imageLoader.DisplayImage(backgroundImageUrl, img);

					page.setBackgroundColor(Color.argb(255, 0, 0, blue));
					container.addView(page);

					return (page);

				} catch (Exception e) {
					System.out.println("error @displaying0: " + e);
					return null;
				}
			} else if (position == 1) {
				try {

					View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_one, container,
							false);
					TextView tv = (TextView) page.findViewById(R.id.txtTitle);

					tv.setText(person.getBio());

					container.addView(page);

					return (page);

				} catch (Exception e) {
					System.out.println("error @displaying1: " + e);
					return null;
				}
			} else if (position == 2) {

				try {
					View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_two, container,
							false);
					ArrayList<Movie> movies = person.getMovies();

					container.addView(page);
					try {

						PersonInfoMoviesAdapter adapter = new PersonInfoMoviesAdapter(
								page.getContext(),
								R.layout.movie_info_cast_adapter, movies);

						ListView listView = (ListView) findViewById(R.id.personslist);
						listView.setAdapter(adapter);
						listView.setOnItemClickListener(mMessageClickedHandler);

					} catch (Exception e) {
						System.out
								.println("error while setting up adapter" + e);
					}

					return (page);

				} catch (Exception e) {
					System.out.println("error @displaying2: " + e);
					return null;
				}

			} else if (position == 3) {
				try {

					final View page = getLayoutInflater().inflate(
							R.layout.movie_info_layout_item_four, container,
							false);

					// TextView tv = (TextView)
					// page.findViewById(R.id.txtTitle);

					// tv.setText("Images Section: Comming Soon..");

					container.addView(page);
					// ArrayList<String> urls = null;
					// overriding my geturl class and replace the postresult
					// method with myown
					class datatest extends getURLdata {
						@Override
						public void postresult(Object result2) {
							String[] tempurls = null;
							try {
								addtoCacheFile((String) result2,
										filePersonImages);
							} catch (Exception e) {
								System.out
										.println("unable to add urls to files");
							}
							try {
								ArrayList<apiImageUrls> arrayurls = parseimages(result2);
								String[] urls = new String[arrayurls.size()];
								for (int x = 0; x < arrayurls.size(); x++) {
									apiImageUrls currentImageUrl = arrayurls
											.get(x);
									urls[x] = (server + currentImageUrl
											.getImageUrlSmall());
								}
								tempurls = urls;
							} catch (Exception e) {
								System.out
										.println("error in forloop position 4 "
												+ e);
							}

							try {
								MainImageAdapter adapter = new MainImageAdapter(
										page.getContext(),
										R.layout.main_layout_back, tempurls);
								// GridView gridView =
								// (GridView)findViewById(R.id.GridView1);
								GridView gridview = (GridView) findViewById(R.id.movie_info_layout_four_gridview1);
								// gridView.setAdapter(adapter);
								gridview.setAdapter(adapter);
								gridview.setOnItemClickListener(mImageClickedHandler);
							} catch (Exception e) {
								System.out
										.println("error while setting up adapter"
												+ e);
							}
						}
					}
					Boolean cacheBoolean = false;
					String res1 = new String();
					// ------------ LOTS OF REPEAT CODE INCOMING HERE
					if (filePersonImages.exists()) {
						FileReader fr = new FileReader(filePersonImages);
						if (fr.read() == -1) {
							System.out.println("EMPTY");
							cacheBoolean = false;
						} else {
							try {
								res1 = readFile(filePersonImages
										.getAbsolutePath());
								cacheBoolean = true;
							} catch (IOException e) {
								System.out
										.println("error while reading cache file after its not empty "
												+ e);
								cacheBoolean = false;
							}
						}

					} else {
						System.out.println("DOES NOT EXISTS");
						cacheBoolean = false;
					}
					if (!cacheBoolean || update_info) {
						System.out.println(image_server);
						datatest geturldata = new datatest();
						geturldata.getData(image_server);
					} else {
						String[] tempurls = null;
						try {
						} catch (Exception e) {
							System.out.println("unable to add urls to files");
						}
						try {
							ArrayList<apiImageUrls> arrayurls = parseimages(res1);
							String[] urls = new String[arrayurls.size()];
							for (int x = 0; x < arrayurls.size(); x++) {
								apiImageUrls currentImageUrl = arrayurls.get(x);
								urls[x] = (server + currentImageUrl
										.getImageUrlSmall());
							}
							tempurls = urls;
						} catch (Exception e) {
							System.out.println("error in forloop position 4 "
									+ e);
						}

						try {
							MainImageAdapter adapter = new MainImageAdapter(
									page.getContext(),
									R.layout.main_layout_back, tempurls);
							// GridView gridView =
							// (GridView)findViewById(R.id.GridView1);
							GridView gridview = (GridView) findViewById(R.id.movie_info_layout_four_gridview1);
							// gridView.setAdapter(adapter);
							gridview.setAdapter(adapter);
							gridview.setOnItemClickListener(mImageClickedHandler);
						} catch (Exception e) {
							System.out.println("error while setting up adapter"
									+ e);
						}
					}

					return (page);

				} catch (Exception e) {
					System.out.println("error @displaying1: " + e);
					return null;
				}
			}

			else
				return null;

		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return (4);
		}

		@Override
		public float getPageWidth(int position) {
			return (1f);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return (view == object);
		}
	}
	
	public void addtoCacheFile(String string, File f) {
		System.out.println("filename" +f.getName());
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					f.getAbsolutePath(), false)));
		} catch (IOException e) {
			System.out.println(e);
		}
		if (out != null)
			out.println(string);
		if (out != null)
			out.close();
	}

}