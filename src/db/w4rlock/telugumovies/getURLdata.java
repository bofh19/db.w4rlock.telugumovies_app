package db.w4rlock.telugumovies;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.util.Log;

public class getURLdata {
	public static final String DEBUG_TAG = "server reponse";
	public String result=null;

	public void getData(String url) {
		new DownloadWebpageText().execute(url);
	}
	
	public void postresult(Object result2){
		result = (String) result2;
		System.out.println("result2 "+result2);
	}
	
	private class DownloadWebpageText extends AsyncTask<Object, Object, Object> {

		@Override
		protected Object doInBackground(Object... params) {
			String data = (String) params[0];
			try {
				return downloadUrl(data);
			} catch (IOException e) {
				return "unable to get the data sry :(";
			}
		}

		@Override
		protected void onPostExecute(Object result) {
			if (result == null || result == " " || result == "") {
				result = "-1";
			} else {
				postresult(result);
				
							}
		}

		private String downloadUrl(String myurl) throws IOException {
			InputStream is = null;

			try {
				System.out.println(myurl);
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);
				// Starts the query
				try {
					conn.connect();
				} catch (Exception e) {
					System.out.println("Error while connection: " + e);
					return " ";
				}
				System.out.println("connection request above");
				int response = conn.getResponseCode();
				Log.d(DEBUG_TAG, " " + response);
				try {
					is = conn.getInputStream();
				} catch (Exception e) {
					System.out
							.println("error while getting inputstream in do background: "
									+ e);
					return " ";
				}
				String contentAsString = readAll(is);
				return contentAsString;

			} catch (Exception e) {
				System.out.println(e);
				System.out.println("x");
				return " ";
			} finally {
				if (is != null) {
					is.close();
				} else {
					return null;
				}
			}
		}

		public String readAll(InputStream stream) throws IOException,
				UnsupportedEncodingException {
			StringBuilder sb = new StringBuilder();
			String s;
			try {
				Reader reader = new InputStreamReader(stream, "UTF-8");
				BufferedReader buf = new BufferedReader(reader);
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);
				}
				return sb.toString();
			} catch (Exception e) {
				System.out.println("error on readall ie catching input stream");
				return " ";
			}
		}
	}

	public void postresult() {
		// TODO Auto-generated method stub
		
	}
}
