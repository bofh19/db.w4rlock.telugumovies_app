package db.w4rlock.telugumovies;

public class ServerVariables {
	public static final String server = "http://db.w4rlock.in";
//	public static final String server = "http://172.16.0.3:8000";
//	public static final String server = "http://192.168.0.104:8000";
//	public static final String server = "http://192.168.1.2:8000";
	public static final String ldburl = server+"/mobile/v1/api/latestdb/";
	public static final String upcomming_movies = server+"/mobile/v1/api/releasing/";
	public static final String intheaters = server+"/mobile/v1/api/intheaters";
	
	
	public static final String youtube_image_url_start = "http://img.youtube.com/vi/"; // + youtube_code+ "/0.jpg"
	public static final String youtube_image_url_end = "/0.jpg";
	public static final String youtube_video_url_start = "http://www.youtube.com/watch?v=";
	public static final String youtube_video_url_end = "";
	
	public static final String releasing_images_url = server+"/mobile/v1/api/releasing_images/";
	
	public static final String search_query_url = server+"/mobile/v1/api/search/all/?q=";
	
}
