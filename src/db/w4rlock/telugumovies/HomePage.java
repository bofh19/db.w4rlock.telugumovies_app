package db.w4rlock.telugumovies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

public class HomePage extends Activity {

	public static final String server = ServerVariables.server;
	public static final String ldburl = ServerVariables.ldburl;
	public static final String upcomming_movies = ServerVariables.upcomming_movies;
	public static final String intheaters = ServerVariables.intheaters;
	
	public final static String URL_MESSAGE = "db.w4rlock.telugumovies.MESSAGE";
 
	
	public final static String latest_db_msg = "Latest Additions to Database";
	public final static String upcomming_msg = "Upcomming Movies";
	public final static String intheaters_msg = "Running in Theaters";
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.homepage, menu);
        return true;
    }
 

	public void getData_latestdb(View view){
    	String[] intent_messages = {server,ldburl,latest_db_msg};
    	Intent intent = new Intent(this, IndexView.class);
    	intent.putExtra(URL_MESSAGE, intent_messages);
    	startActivity(intent);

    	
    	
//    	if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
//    		
//    	}
//    	 v.setDrawingCacheEnabled(true);
//         v.setPressed(false);
//         v.refreshDrawableState();
//         Bitmap bm = v.getDrawingCache();
//         Canvas c = new Canvas(bm);
//         //c.drawARGB(255, 255, 0, 0);
//         ActivityOptions opts = ActivityOptions.makeThumbnailScaleUpAnimation(
//                 v, bm, 0, 0);
//         // Request the activity be started, using the custom animation options.
//         startActivity(new Intent(Animation.this, AlertDialogSamples.class), opts.toBundle());
//         v.setDrawingCacheEnabled(false);
    	
    }
    
    public void getData_upcomming_movies(View view){
    	String[] intent_messages = {server,upcomming_movies,upcomming_msg};
    	Intent intent = new Intent(this, IndexView.class);
    	intent.putExtra(URL_MESSAGE, intent_messages);
    	startActivity(intent);
    }
    
    public void getData_intheaters(View view){
    	String[] intent_messages = {server,intheaters,intheaters_msg};
    	Intent intent = new Intent(this, IndexView.class);
    	intent.putExtra(URL_MESSAGE, intent_messages);
    	startActivity(intent);
    }

        
  }
