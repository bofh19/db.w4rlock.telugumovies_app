package db.w4rlock.telugumovies;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;
import db.w4rlock.telugumovies.Models.apiImageUrls;
import db.w4rlock.telugumovies.utils.Utils;

@SuppressLint("NewApi")
public class ImagesViewFull extends Activity {
	
	public final static String IMAGE_URLs_MESSAGE = "db.w4rlock.telugumovies.IMAGE_URLs_MESSAGE";
	public final static String IMAGE_URLs_ID = "db.w4rlock.telugumovies.IMAGE_URLs_ID";
	public String callerClassName = null;
	public String cur_img_url = null;
	public WebView webView = null;
	private File cacheDir;
	private Toast toast;
	private File f;
	private ArrayList<apiImageUrls> movie_images = null;
	private int image_id = -1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_images_view_full);
		Intent intent = getIntent();
		callerClassName = intent.getStringExtra("caller");
		movie_images = (ArrayList<apiImageUrls>) intent.getSerializableExtra(IMAGE_URLs_MESSAGE);
		image_id = intent.getIntExtra(IMAGE_URLs_ID, -1);
		cur_img_url = ServerVariables.server+movie_images.get(image_id).getImageUrlActual();
	    toast = Toast.makeText( this  , "Loading View" , Toast.LENGTH_SHORT );
	    toast.show();
		webView = (WebView) findViewById(R.id.WebView);
		webView.getSettings().setSupportZoom(true);  
		webView.getSettings().setBuiltInZoomControls(true);
		final ProgressBar progess = (ProgressBar) findViewById(R.id.ProgressBar);
		webView.setWebViewClient(new WebViewClient() {
		  public void onPageStarted(WebView view, String url, Bitmap favicon) {
		    progess.setVisibility(View.VISIBLE);
		  }
		  public void onPageFinished(WebView view, String url) {
		    progess.setVisibility(View.GONE);
		  }
		});
		webView.loadUrl(cur_img_url);
		this.setTitle("Images");
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_images_view_full, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId(
				)) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			finish();
//			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.set_as_image:
//			System.out.println("in set as");
			Intent setAs = new Intent(Intent.ACTION_ATTACH_DATA); setAs.setType("image/jpg");
			startActivity(setAs);
			return true;
		case R.id.download_img:
			save_image();
			return true;
		case R.id.previous_img:
			displayAnotherimage(-1);
			return true;
		case R.id.next_img:
			displayAnotherimage(1);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void displayAnotherimage(int i) {
		image_id = image_id +i;
		if(image_id>=0 && image_id < movie_images.size()){
			cur_img_url = ServerVariables.server+movie_images.get(image_id).getImageUrlActual();
			final ProgressBar progess = (ProgressBar) findViewById(R.id.ProgressBar);
			webView.setWebViewClient(new WebViewClient() {
			  public void onPageStarted(WebView view, String url, Bitmap favicon) {
			    progess.setVisibility(View.VISIBLE);
			  }
			  public void onPageFinished(WebView view, String url) {
			    progess.setVisibility(View.GONE);
			  }
			});
			webView.loadUrl(cur_img_url);
		}else if(image_id<0){
			toast.setText("No More Images Try Pressing Next");
			toast.show();
		}else if(image_id >= movie_images.size()){
			toast.setText("No More Images Try Pressing Previous");
			toast.show();
		}
	}

	private void save_image() {

		toast.setText("Downloading File to TeluguMoviesDownloads Folder");
		toast.show();
		f = getFile(cur_img_url);
		new DownloadWebpageText().execute(cur_img_url);
		
	}
	
	private File getFile(String url){
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"TeluguMoviesDownloads");
        else
            cacheDir=this.getCacheDir();
        if(!cacheDir.exists())
            cacheDir.mkdirs();
        String filename=String.valueOf(url.hashCode())+".jpg";
        File f = new File(cacheDir, filename);
		return f;
	}
	
	private class DownloadWebpageText extends AsyncTask<Object, Object, Object> {

		@Override
		protected Object doInBackground(Object... params) {
			String data = (String) params[0];
			try {
				return downloadUrl(data);
			} catch (IOException e) {
				return "unable to get the data sry :(";
			}
		}

		@Override
		protected void onPostExecute(Object result) {
			toast.setText("Image Saved to TeluguMoviesDownloads Image File Name "+f.getName());
			toast.show();
		}

		private String downloadUrl(String myurl) throws IOException {
			try {
	            URL imageUrl = new URL(myurl);
	            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
	            conn.setConnectTimeout(30000);
	            conn.setReadTimeout(30000);
	            conn.setInstanceFollowRedirects(true);
	            InputStream is=conn.getInputStream();
	            OutputStream os = new FileOutputStream(f);
	            Utils.CopyStream(is, os);
	            os.close();
	        } catch (Exception ex){
	           System.out.println("error while download and saving");
	           System.out.println(ex);
	           return null;
	        }
			return "";
		}
	}




}
