package db.w4rlock.telugumovies;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import db.w4rlock.telugumovies.Adapters.SearchAdapter;
import db.w4rlock.telugumovies.utils.getURLdata;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

public class SearchActivity extends ListActivity{
	private SearchAdapter adapter;
	public final static String PERSON_ID_MESSAGE = "db.w4rlock.telugumovies.MESSAGE";
	public final static String MOVIE_ID_MESSAGE = "db.w4rlock.telugumovies.MESSAGE";
	//private ListView listView;
	public static final String server = ServerVariables.server;
	public Toast loadingtoast;
	public ArrayList<String[]> result_arraylist = new ArrayList<String[]>();
	 public void onCreate(Bundle savedInstanceState) { 
	      super.onCreate(savedInstanceState); 
	      handleIntent(getIntent()); 
	      
	      LayoutInflater mInflater=LayoutInflater.from(this);
	      View view=mInflater.inflate(R.xml.search_loading_dialog,null);
	      loadingtoast=new Toast(this);
	      loadingtoast.setView(view);
	      loadingtoast.setDuration(Toast.LENGTH_LONG);
	   }

	   public void onNewIntent(Intent intent) { 
	      setIntent(intent); 
	      handleIntent(intent); 
	   } 
	   @SuppressLint("NewApi")
	@Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	    	MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.search_activity, menu);
	        
	        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	        SearchView searchView = (SearchView) menu.findItem(R.id.grid_default_search).getActionView();
	        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
	        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

	        return true;
	    }
	   @Override
	    public boolean onOptionsItemSelected(MenuItem item){
	    	switch (item.getItemId()) {
			case R.id.search_button:
				onSearchRequested();
			default:
				return super.onOptionsItemSelected(item);
			}
	    }
	   public void onListItemClick(ListView l, 
	      View v, int position, long id) { 
	      // call detail activity for clicked entry
		   System.out.println(position);
		   System.out.println(result_arraylist.get(position)[0]);
		   if(result_arraylist.get(position)[2].equals("movie")){
			   Intent intent = new Intent(this, MovieInfo.class);
				String[] intent_messages = { result_arraylist.get(position)[1], server };
				intent.putExtra(MOVIE_ID_MESSAGE, intent_messages);
				startActivity(intent);
		   }
		   else if(result_arraylist.get(position)[2].equals("person")){
			   Intent intent = new Intent(this, PersonInfo.class);
				String[] intent_messages = {result_arraylist.get(position)[1] , server };
				intent.putExtra(PERSON_ID_MESSAGE, intent_messages);
				startActivity(intent);
		   }
		   
	   } 

	   private void handleIntent(Intent intent) { 
	      if (Intent.ACTION_SEARCH.equals(intent.getAction())) { 
	         String query = intent.getStringExtra(SearchManager.QUERY);
	         query = query.replaceAll(" ", "%20");
	         loadingtoast.show();
	         setListAdapter(null);
	         doSearch(query); 
	      } 
	   }    

	   private void doSearch(String queryStr) { 
			class datatest extends getURLdata {
				@Override
				public void postresult(Object result2) {
					//System.out.println((String)result2);
					arrange_results((String)result2);
				}
			}
			datatest geturldata = new datatest();
			geturldata.getData(ServerVariables.search_query_url+queryStr);
	   }
	   public void arrange_results(String result){
		   if(result_arraylist.size()>0){
			   result_arraylist.clear();
		   }
		   try {
			   JSONObject newobj = new JSONObject(result);
				JSONArray entries = new JSONArray();
				entries = newobj.getJSONArray("results");
				for (int i = 0; i < entries.length(); i++) {
					try {
						JSONObject thisobj = (JSONObject) entries.get(i);
						String[] this_result;
						this_result = new String[4];
						this_result[0] = (String)(thisobj.get("label"));
						this_result[1] = ((Integer)(thisobj.get("id"))).toString();
						this_result[2] = (String)(thisobj.get("desc"));
						this_result[3] = (String)(ServerVariables.server+thisobj.get("icon"));
						result_arraylist.add(this_result);
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				//System.out.println(result_arraylist.toArray());
				String[][] results_array = new String[result_arraylist.size()][4];
				for (int i=0;i<result_arraylist.size();i++){
					results_array[i] = result_arraylist.get(i);
				}
        		try {
        			adapter = new SearchAdapter(this, R.xml.search_result_adapter,results_array);
        			loadingtoast.cancel();
        			setListAdapter(adapter);
        		} catch (Exception e) {
        			System.out.println("error herex");
        			System.out.println(e);
        		}
		   }catch(Exception e){
			   System.out.println(e);
			   System.out.println("error while parsing results");
		   }
	   }
	   
	   
}
