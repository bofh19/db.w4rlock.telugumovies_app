package db.w4rlock.telugumovies;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PersonInfoMoviesAdapter extends ArrayAdapter<Movie> {
	Context context;
	int layoutResourceId;
	ArrayList<Movie> movies = null;
	public ImageLoader imageLoader;
	
	public PersonInfoMoviesAdapter(Context context, int layoutResourceId,ArrayList<Movie> movies){
		super(context,layoutResourceId,movies);
		
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.movies = movies;
		imageLoader = new ImageLoader(context.getApplicationContext());
	}
	
	@Override
	public View getView(int position,View convertView, ViewGroup parent){
		View row = convertView;
		
		MovieInfoPersonHolder holder = null;
		
		if(row == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent,false);
			holder = new MovieInfoPersonHolder();
			
			holder.img = (ImageView)row.findViewById(R.id.imgIcon);
			holder.titletxt = (TextView)row.findViewById(R.id.txtTitle);
			holder.proftxt = (TextView)row.findViewById(R.id.professions_list);
			row.setTag(holder);
		}else{
			holder = (MovieInfoPersonHolder)row.getTag();
		}
		
		Movie movie = movies.get(position);
		
		try{
			imageLoader.DisplayImage(movie.getIcon_small(), holder.img);
		}catch(Exception e){
			System.out.println("error while displaying profile image@: "+position+" "+e);
		}
		try{
			holder.titletxt.setText(movie.getTitle());
		}catch(Exception e){
			System.out.println("error while displaying person name@: "+position+" "+e);
		}
		
		holder.proftxt.setText("");
		
		
		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.slide_out_right);
        row.startAnimation(animation);
		return row;
		
	}
	
	static class MovieInfoPersonHolder{
		ImageView img;
		TextView titletxt;
		TextView proftxt;
	}
	
}
