package db.w4rlock.telugumovies;

import java.io.Serializable;


public class apiImageUrls implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4676853676922776087L;
	/**
	 * 
	 */
	public String imageUrlSmall;
	public String imageUrlActual;
	public String getImageUrlSmall() {
		return imageUrlSmall;
	}
	public void setImageUrlSmall(String imageUrlSmall) {
		this.imageUrlSmall = imageUrlSmall;
	}
	public String getImageUrlActual() {
		return imageUrlActual;
	}
	public void setImageUrlActual(String imageUrlActual) {
		this.imageUrlActual = imageUrlActual;
	}
}