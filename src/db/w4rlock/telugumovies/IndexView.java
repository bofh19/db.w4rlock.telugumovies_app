package db.w4rlock.telugumovies;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import db.w4rlock.telugumovies.Adapters.MovieAdapter;
import db.w4rlock.telugumovies.Models.Movie;
import db.w4rlock.telugumovies.utils.FileCache;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class IndexView extends Activity {

	private static final String DEBUG_TAG = "Server Response";
	public final static String MOVIE_ID_MESSAGE = "db.w4rlock.telugumovies.MESSAGE";

	private TextView textView;
	private MovieAdapter adapter;
	private ListView listView;
	private ArrayList<Movie> movie_data;
	private String server = null;
	FileCache fileCache;
	public File f = null;
	String[] intent_mes_update = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_index_view);
		textView = (TextView) findViewById(R.id.myText);
		movie_data = new ArrayList<Movie>();

		textView.setText("checking net");

		Intent intent = getIntent();
		String[] intent_messages = intent
				.getStringArrayExtra(HomePage.URL_MESSAGE);
		intent_mes_update = new String[intent_messages.length + 1];
		intent_mes_update[0] = intent_messages[0];
		intent_mes_update[1] = intent_messages[1];
		intent_mes_update[2] = intent_messages[2];
		intent_mes_update[3] = "1";
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		String ldburl = intent_messages[1];
		server = intent_messages[0];
		this.setTitle(intent_messages[2]);

		fileCache = new FileCache(getApplicationContext());
		f = fileCache.getFile(ldburl);
		Boolean cachecheck = false;

		if (intent_messages.length == 4) {
			cachecheck = false;
		} else {
			try {
				cachecheck = runFromFile(f);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				cachecheck = false;
				e1.printStackTrace();
			}
		}

		if (!cachecheck) {

			if (networkInfo != null && networkInfo.isConnected()) {
				textView.setText("loading data...");
				try {
					textView.setText("contacting server");
					new DownloadWebpageText().execute(ldburl);
				} catch (Exception e) {
					System.out.println(e);
					textView.setText("Error On Getting Data");
				}
			} else {
				textView.setText("no network connection avilable");
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_index_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_upcomming_movies:
			try {
				String[] intent_messages = { server, HomePage.upcomming_movies,
						HomePage.upcomming_msg };
				Intent intent = new Intent(this, IndexView.class);
				intent.putExtra(HomePage.URL_MESSAGE, intent_messages);
				finish();
				startActivity(intent);
			} catch (Exception e) {
				System.out.println("Error while changing activity: " + e);
			}
			return true;
		case R.id.menu_latest_db:
			try {
				String[] intent_messages = { server, HomePage.ldburl,
						HomePage.latest_db_msg };
				Intent intent = new Intent(this, IndexView.class);
				intent.putExtra(HomePage.URL_MESSAGE, intent_messages);
				finish();
				startActivity(intent);
			} catch (Exception e) {
				System.out.println("Error while changing activity: " + e);
			}
			return true;
		case R.id.menu_in_theaters:
			try {
				String[] intent_messages = { server, HomePage.intheaters,
						HomePage.intheaters_msg };
				Intent intent = new Intent(this, IndexView.class);
				intent.putExtra(HomePage.URL_MESSAGE, intent_messages);
				finish();
				startActivity(intent);
			} catch (Exception e) {
				System.out.println("Error while changing activity: " + e);
			}
			return true;
		case R.id.menu_main_page:
			try {
				Intent intent = new Intent(this, MainActivity.class);
				startActivity(intent);
			} catch (Exception e) {
				System.out.println("Error while changing activity: " + e);
			}
			return true;
		case R.id.menu_index:
			try {
				Intent intent = new Intent(this, HomePage.class);
				startActivity(intent);
			} catch (Exception e) {
				System.out.println("Error while changing acitivty: " + e);
			}
			return true;
		case R.id.menu_update:
			Intent intent = new Intent(this, IndexView.class);
			intent.putExtra(HomePage.URL_MESSAGE, intent_mes_update);
			finish();
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private static String readFile(String path) throws IOException {
		FileInputStream stream = new FileInputStream(new File(path));
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}

	public boolean runFromFile(File f) throws IOException {
		String res = "";
		if (f.exists()) {
			FileReader fr = new FileReader(f);
			if (fr.read() == -1) {
				System.out.println("EMPTY");
				return false;
			} else {
				try {
					res = readFile(f.getAbsolutePath());
				} catch (IOException e) {
					System.out
							.println("error while reading cache file after its not empty "
									+ e);
					return false;
				}
			}
		} else {
			System.out.println("DOES NOT EXISTS");
			return false;
		}
		postresult(res);
		return true;
	}

	public void postresult(Object result) {
		String res = (String) result;
		String res_id = " ";
		textView.setText("data acquired trying to parse result now");
		try {
			JSONArray entries = new JSONArray(res);

			for (int i = 0; i < entries.length(); i++) {
				JSONObject thisobj = (JSONObject) entries.get(i);
				// System.out.println(thisobj.get("id"));
				// res_id = res_id +"\n"+ thisobj.get("banner_image");

				// this_m.setText((String) thisobj.get("name"));
				// adapter.add((String) thisobj.get("name"));
				try {
					Movie this_movie = new Movie();
					ArrayList<String> temp_actors = new ArrayList<String>();
					this_movie.icon = server
							+ (String) (thisobj.get("banner_image_small"));
					this_movie.title = (String) (thisobj.get("name"));
					this_movie.days_left = (Integer) (thisobj.get("days_left"));

					this_movie.movie_id = (Integer) (thisobj.get("id"));
					try {
						JSONArray entries_persons = (JSONArray) thisobj
								.get("persons");
						for (int x = 0; x < entries_persons.length(); x++) {
							JSONObject thisobj_person = (JSONObject) entries_persons
									.get(x);
							temp_actors
									.add((String) thisobj_person.get("name"));
						}
					} catch (Exception e) {
						System.out.println("error while catching persons" + e);
						temp_actors = null;
					}
					this_movie.actors = temp_actors;
					movie_data.add(this_movie);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
			textView.setText(res_id);
			check(movie_data);
		} catch (JSONException e) {
			textView.setText("unable-to-convert-acquired-data-to-json-object");
			Log.i("error@convert stringtojsonobject",
					"unable-to-convert-to-json-object");
		}

		try {
			View loadingbar = (View) findViewById(R.id.progress_large);
			loadingbar.setVisibility(View.GONE);
		} catch (Exception e) {
			System.out.println("Error while removin loading bar " + e);
		}
	}

	public void check(ArrayList<Movie> movie_arraylist) {
		Movie[] movie_array = movie_arraylist.toArray(new Movie[movie_arraylist
				.size()]);
		try {
			adapter = new MovieAdapter(this, R.layout.m_layout_item_row,
					movie_array);
			listView = (ListView) findViewById(R.id.mylist);

			// to get header just inflate the header and then use
			// listview.addheader command .. <just so i wont forget>

			listView.setAdapter(adapter);
			listView.setOnItemClickListener(mMessageClickedHandler);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// Create a message handling object as an anonymous class.
	private OnItemClickListener mMessageClickedHandler = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
				long arg3) {
			System.out.println(parents);
			System.out.println(arg1);
			System.out.println(arg2);
			System.out.println(arg3);
			Movie clicked_movie = movie_data.get(arg2);
			System.out.println(clicked_movie.title);
			Integer this_movie_id = (Integer) clicked_movie.movie_id;

			Intent intent = new Intent(parents.getContext(), MovieInfo.class);
			String[] intent_messages = { this_movie_id.toString(), server };
			intent.putExtra(MOVIE_ID_MESSAGE, intent_messages);
			startActivity(intent);

		}
	};

	private class DownloadWebpageText extends AsyncTask<Object, Object, Object> {

		@Override
		protected Object doInBackground(Object... params) {
			String data = (String) params[0];
			try {
				return downloadUrl(data);
			} catch (IOException e) {
				return "unable to get the data sry :(";
			}
		}

		@Override
		protected void onPostExecute(Object result) {

			if (result == null || result == " " || result == "") {
				textView.setText("result is broken are you online ?");
				try {
					View loadingbar = (View) findViewById(R.id.progress_large);
					loadingbar.setVisibility(View.GONE);
				} catch (Exception e) {
					System.out.println("Error while removin loading bar " + e);
				}
			} else {
				postresult(result);
			}
		}

		private String downloadUrl(String myurl) throws IOException {
			InputStream is = null;

			try {
				System.out.println(myurl);
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);
				// Starts the query
				try {
					conn.connect();
				} catch (Exception e) {
					System.out.println("Error while connection " + e);
					return " ";
				}
				System.out.println("connection request above");
				int response = conn.getResponseCode();
				Log.d(DEBUG_TAG, " " + response);
				try {
					is = conn.getInputStream();
				} catch (Exception e) {
					System.out
							.println("error while getting inputstream in do background "
									+ e);
					return " ";
				}
				String contentAsString = readAll(is);
				return contentAsString;

			} catch (Exception e) {
				System.out.println(e);
				System.out.println("x");
				return " ";
			} finally {
				if (is != null) {
					is.close();
				} else {
					return null;
				}
			}
		}

		public String readAll(InputStream stream) throws IOException,
				UnsupportedEncodingException {
			StringBuilder sb = new StringBuilder();
			String s;
			try {
				Reader reader = new InputStreamReader(stream, "UTF-8");
				BufferedReader buf = new BufferedReader(reader);
				while (true) {
					s = buf.readLine();
					if (s == null || s.length() == 0)
						break;
					sb.append(s);
				}
				addtoCacheFile(sb.toString(), f);
				return sb.toString();
			} catch (Exception e) {
				System.out.println("error on readall ie catching input stream");
				return " ";
			}
		}
	}

	public void addtoCacheFile(String string, File f) {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(
					f.getAbsolutePath(), false)));
		} catch (IOException e) {
			System.out.println(e);
		}
		if (out != null)
			out.println(string);
		if (out != null)
			out.close();
	}
}
