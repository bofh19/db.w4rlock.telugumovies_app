package db.w4rlock.telugumovies.Adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import db.w4rlock.telugumovies.R;
import db.w4rlock.telugumovies.Models.MoviePersons;
import db.w4rlock.telugumovies.utils.ImageLoader;

public class MovieInfoPersonsAdapter extends ArrayAdapter<MoviePersons> {
	Context context;
	int layoutResourceId;
	ArrayList<MoviePersons> moviePersons = null;
	public ImageLoader imageLoader;
	
	public MovieInfoPersonsAdapter(Context context, int layoutResourceId,ArrayList<MoviePersons> moviePersons){
		super(context,layoutResourceId,moviePersons);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.moviePersons = moviePersons;
		imageLoader = new ImageLoader(context.getApplicationContext());
	}
	
	@Override
	public View getView(int position,View convertView, ViewGroup parent){
		View row = convertView;
		
		MovieInfoPersonHolder holder = null;
		
		if(row == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent,false);
			holder = new MovieInfoPersonHolder();
			
			holder.img = (ImageView)row.findViewById(R.id.imgIcon);
			holder.titletxt = (TextView)row.findViewById(R.id.txtTitle);
			holder.proftxt = (TextView)row.findViewById(R.id.professions_list);
			row.setTag(holder);
		}else{
			holder = (MovieInfoPersonHolder)row.getTag();
		}
		
		MoviePersons moviePerson = moviePersons.get(position);
		
		try{
			imageLoader.DisplayImage(moviePerson.getProfileImageUrlSmall(), holder.img);
		}catch(Exception e){
			System.out.println("error while displaying profile image@: "+position+" "+e);
		}
		try{
			holder.titletxt.setText(moviePerson.getName());
		}catch(Exception e){
			System.out.println("error while displaying person name@: "+position+" "+e);
		}
		try{
			ArrayList<String> professions_array=moviePerson.getProfessions();
			String professions = "";
			for(int prof=0;prof<professions_array.size();prof++){
				professions = professions + professions_array.get(prof)+"\n";
			}
			holder.proftxt.setText(professions);
		}catch(Exception e){
			System.out.println("Error while getting professions@: "+position+" "+e);
		}
		
		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.slide_out_right);
        row.startAnimation(animation);
		return row;
		
	}
	
	static class MovieInfoPersonHolder{
		ImageView img;
		TextView titletxt;
		TextView proftxt;
	}
	
}
