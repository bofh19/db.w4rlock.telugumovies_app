package db.w4rlock.telugumovies.Adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import db.w4rlock.telugumovies.R;
import db.w4rlock.telugumovies.Models.Movie;
import db.w4rlock.telugumovies.utils.ImageLoader;


public class MovieAdapter extends ArrayAdapter<Movie>{
	Context context;
	int layoutResourceId;
	Movie data[] = null;
	public ImageLoader imageLoader; 

	public MovieAdapter(Context context, int layoutResourceId, Movie[] data){
		super(context,layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		imageLoader=new ImageLoader(context.getApplicationContext());
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		MovieHolder holder = null;
		if(row == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
			
			row = inflater.inflate(layoutResourceId, parent,false);
			
			holder = new MovieHolder();
			holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
			holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
			holder.days_left = (TextView)row.findViewById(R.id.days_left);
			holder.actors = (TextView)row.findViewById(R.id.actors_list);
			row.setTag(holder);
			
		}else{
			holder = (MovieHolder)row.getTag();
		}
		Movie movie = data[position];
		holder.txtTitle.setText(movie.title);
		if(movie.days_left>-250 && movie.days_left<250){
		
			if(movie.days_left<0){
				holder.days_left.setText(Html.fromHtml("Releasing in <font color=red>"+(Integer)movie.days_left * -1 + "</font> days"));
			}else{
				holder.days_left.setText("Released "+movie.days_left+" days back");
			}
//			holder.days_left.setText("days left: "+movie.days_left);	
		}else{
			holder.days_left.setVisibility(View.GONE);
		}
		
		String this_actors = "";
		
		try{
			if(movie.actors != null){	
				for(int count=0;count<movie.actors.size();count++){
					
					this_actors += (String)movie.actors.get(count) + "<br/>";
				}
			}
			}catch(Exception e){
				System.out.println("Error near actors "+e);
			}
		holder.actors.setText(Html.fromHtml(this_actors));
		
		try{
			String url = movie.icon;
//			System.out.println(url);
			imageLoader.DisplayImage(url, holder.imgIcon);
//			new DownloadImageTask().execute(url);
		} catch(Exception e){
			System.out.println("while placing image error: "+e);
		}
		
		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.slide_out_right);
        row.startAnimation(animation);
		return row;
	}
	

	
	static class MovieHolder
	{
		ImageView imgIcon;
		TextView txtTitle;
		TextView days_left;
		TextView actors;
	}
}
