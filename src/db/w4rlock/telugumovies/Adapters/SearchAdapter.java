package db.w4rlock.telugumovies.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import db.w4rlock.telugumovies.R;
import db.w4rlock.telugumovies.utils.ImageLoader;

public class SearchAdapter extends ArrayAdapter<String[]>{
	Context context;
	int layoutResourceId;
	String[][] data = null;
	public ImageLoader imageLoader; 

	public SearchAdapter(Context context, int layoutResourceId, String[][] data){
		super(context,layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		imageLoader=new ImageLoader(context.getApplicationContext());
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		ResultHolder holder = null;
		if(row == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
			
			row = inflater.inflate(layoutResourceId, parent,false);
			
			holder = new ResultHolder();
			holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
			holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
			holder.desc = (TextView)row.findViewById(R.id.result_desc);
			row.setTag(holder);
			
		}else{
			holder = (ResultHolder)row.getTag();
		}
		String[] this_result = data[position];
		holder.txtTitle.setText((String)this_result[0]);
		holder.desc.setText(this_result[2]);
		try{
			String url = this_result[3];
//			System.out.println(url);
			imageLoader.DisplayImage(url, holder.imgIcon);
//			new DownloadImageTask().execute(url);
		} catch(Exception e){
			System.out.println("while placing image error: "+e);
		}
		
		Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.slide_out_right);
        row.startAnimation(animation);
		return row;
	}
	

	
	static class ResultHolder
	{
		ImageView imgIcon;
		TextView txtTitle;
		TextView desc;
	}
}
